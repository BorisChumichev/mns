import './example.styl'
import { render } from 'react-dom'
import * as React from 'react'
const Writing = require('../lib/index')
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import writingApp from '../lib/reducers'
import { addNote } from '../lib/actions'
import { fetchParagraphsFromElement } from '../lib/utils'
import notes from './notes'

const store = createStore(writingApp)
  , data = fetchParagraphsFromElement(document.querySelector('.writing'))

notes.forEach(note => store.dispatch(addNote(note)))

render(
  <Provider store={store}>
    <Writing paragraphs={data} />
  </Provider>, document.getElementById('root')
)
