import 'lodash'

export function fetchParagraphsFromElement({ children }) {
  let paragraphsToRegister = children.length
    , paragraphs = []

  while (paragraphsToRegister--)
    paragraphs = [ children[paragraphsToRegister].innerHTML ].concat(paragraphs)

  return paragraphs
}

export function wrapTextInSpan(text, { range }, className) {
  if (_.get(range, 'length')) {
    range = _.sortBy(range, n => n)
    return `${text.slice(0, range[0])}<span class='${className}'>${text.slice(range[0], range[1])}</span>${text.slice(range[1])}`
  } else {
    return text
  }
}
