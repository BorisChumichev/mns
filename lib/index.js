import React, { Component } from 'react'
import 'lodash'
import Paragraph from './blocks/paragraph'
import Cursor from './blocks/cursor'
import { connect } from 'react-redux'
import
  { focusOnParagraph
  , dropFocus
  , setSelection
  , clearSelection
  , showForm
  , addNote
  , setUserSelection
  , removeUserSelection
  , highlightText
  , flushHighlight
  , hideForm } from './actions'

class Writing extends Component {
  handleClick() {
    if (this.props.paragraphFocus !== null)
      this.props.dispatch(dropFocus())

    if (window.getSelection().isCollapsed)
      this.props.dispatch(clearSelection())
  }

  handleSelect(paragraphId, selection) {
    selection.paragraphId = paragraphId
    const bdRect = selection.getRangeAt(0).getBoundingClientRect()
    selection.hcenter = bdRect.width * .5 + bdRect.left
    selection.top = window.scrollY + bdRect.top
    this.props.dispatch(setSelection(selection))
  }

  handleCursorClick() {
    const { selection, dispatch } = this.props
    if (this.props.paragraphFocus !== selection.paragraphId)
      dispatch(focusOnParagraph(selection.paragraphId))
    dispatch(showForm())
    dispatch(setUserSelection(selection))
  }

  render() {
    const
      { paragraphFocus
      , notes
      , dispatch
      , paragraphs
      , selection
      , highlight
      , userSelection
      , notesForm } = this.props

    return (
      <div>
        <div onClick={() => this.handleClick()}>
          <div className='writing'>
            {paragraphs.map((paragraph, i) =>
              <Paragraph
                key={i}
                id={i}
                text={paragraph}
                onNewNote={(note) => dispatch(
                  addNote(_.extend(note, { origin: i }))
                )}
                onEnterNote={note => dispatch(
                  highlightText(_.pick(note, 'range', 'origin'))
                )}
                userSelection={userSelection && userSelection.paragraphId === i && userSelection}
                highlight={highlight && highlight.origin === i && highlight}
                onLeaveNote={() => dispatch(flushHighlight())}
                onClearFocus={() => dispatch(dropFocus())}
                onShowForm={() => dispatch(showForm())}
                notesForm={notesForm}
                onTextSelect={(selection) => this.handleSelect(i, selection)}
                faded={(paragraphFocus !== null) && (paragraphFocus !== i)}
                onFocus={() => dispatch(focusOnParagraph(i))}
                focus={i === paragraphFocus}
                notes={_.filter(notes, { origin: i })} />
            )}
          </div>
        </div>
        <Cursor sel={selection} us={userSelection} onClick={() => this.handleCursorClick()} />
      </div>
    )
  }
}

//expose data from redux store
function select(state) {
  return (
    { paragraphFocus: state.paragraphFocus
    , selection: state.selection
    , notes: state.notes
    , highlight: state.highlight
    , userSelection: state.userSelection
    , notesForm: state.notesForm }
  )
}

export default connect(select)(Writing)
