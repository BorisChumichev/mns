import './paragraph-navigation.styl'
import React, { Component } from 'react'

export default class paragraphNavigation extends Component {
  render() {
    let { note, hidden, onItemSelect } = this.props
    return (
      <div className={`paragraphNavigation ${hidden ? 'paragraphNavigation_hidden' : ''}`}>
        <div onClick={() => onItemSelect('leave-note')} className='paragraphNavigation-item'>Add note</div>
        <div onClick={() => onItemSelect('collapse-notes')} className='paragraphNavigation-item'>Hide notes</div>
      </div>
    )
  }
}
