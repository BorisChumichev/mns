import './note.styl'
import React, { Component } from 'react'

export default class Note extends Component {
  render() {
    let { note, hidden, onEnterNote, onLeaveNote } = this.props
      , coordinates = `${require('moment')(note.date).format('ddd, ha')} by ${note.author}:`
      , isNew = (Date.now() - note.date) < 500
      , className = `note ${ hidden ? 'note_hidden' : '' } ${isNew ? 'note_new' : ''}`

    return (
      <div onMouseEnter={() => onEnterNote(note)}
        onMouseLeave={() => onLeaveNote(note)}
        className={className}>
        <div className='note-coordinates'>{coordinates}</div>
        <div className='note-text'>{note.text}</div>
      </div>
    )
  }
}
