import React, { Component } from 'react'
import NotesList from './notes-list'
import { wrapTextInSpan } from '../utils'
import './paragraph.styl'
import 'lodash'

export default class Paragraph extends Component {
  handleSelection(evt) {
    const sel = window.getSelection()
      , hasParagraphAsParent = sel.anchorNode.parentElement === this.refs.paragraph
      , isInOneNode = sel.anchorNode === sel.focusNode
      , isNotTooSmall = Math.abs((sel.anchorOffset - sel.focusOffset)) > 2

    if (isInOneNode && isNotTooSmall && hasParagraphAsParent)
      this.props.onTextSelect(sel)
  }

  stopBubblingIfFocused(evt) {
    if (this.props.focus)
      evt.stopPropagation()
  }

  render() {
    let { text, notes, focus, faded, notesForm, highlight, userSelection } = this.props
      , us = userSelection
      , className = `paragraph ${faded ? 'paragraph_faded' : ''} ${focus ? 'paragraph_focused' : ''}`
      , innerHTML = { __html: (highlight && highlight.range.length)
        ? wrapTextInSpan(text, highlight, 'paragraph-highlight')
        : userSelection
          ? wrapTextInSpan(text, { range: [ us.anchorOffset, us.focusOffset ] }, 'paragraph-highlight')
          : text
      }

    return (
      <div onClick={(evt) => this.stopBubblingIfFocused(evt)} className={className}>
        <p ref='paragraph'
          onMouseUp={() => this.handleSelection()}
          dangerouslySetInnerHTML={innerHTML} />
        <NotesList {...this.props} />
      </div>
    )
  }
}

