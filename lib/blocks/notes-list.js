import './notes-list.styl'

import React, { Component } from 'react'
import 'lodash'
import Note from './note'
import ParapraphNavigation from './paragraph-navigation'

export default class NotesList extends Component {
  constructor(props) {
    super(props)
    this.state = { showSubmit: false }
  }

  publishNote() {
    const getRange = () => this.props.userSelection
      ? [ this.props.userSelection.anchorOffset, this.props.userSelection.focusOffset ]
      : []

    this.props.onNewNote(
      { text: this.refs.input.value
      , type: 'note'
      , date: Date.now()
      , author: 'you'
      , range: getRange() }
    )
    this.refs.input.value = ''
    this.setState({ showSubmit: false })
  }

  handleUserInput(evt) {
    this.setState({
      showSubmit: evt.target.value.length > 6
    })
  }

  handleNavigation(itemId) {
    itemId === 'leave-note'
      ? (this.props.onShowForm(), this.refs.input.focus())
      : this.props.onClearFocus()
  }

  render() {
    let
      { focus
      , notes
      , onFocus
      , faded
      , onEnterNote
      , onLeaveNote
      , notesForm } = this.props

    return (
      <div className={`notesList ${focus ? '' : 'notesList_collapsed'} ${(faded ? 'notesList_faded' : '')}`}>
        {!!notes.length && <div onClick={onFocus} className='notesList-counter'>{notes.length}</div>}
        <ParapraphNavigation hidden={!focus} onItemSelect={itemId => this.handleNavigation(itemId)} />
        <textarea
          ref='input'
          className={`notesList-input ${(!focus || !notesForm) ? 'notesList-input_hidden' : this.refs.input.focus()}`}
          onChange={(evt) => this.handleUserInput(evt)}
          placeholder='Write a note and press enter…'
          />
        <div
          onClick={() => this.publishNote()}
          className={`notesList-sbmt ${(!focus || !notesForm || !this.state.showSubmit) ? 'notesList-sbmt_hidden' : ''}`}>
          Publish note
        </div>
        {_(notes).sortBy('date').reverse().value().map(
          (note, i) => <Note
              key={i}
              note={note}
              hidden={!focus}
              onEnterNote={onEnterNote}
              onLeaveNote={onLeaveNote} />
        )}
      </div>
    )
  }
}
