import './cursor.styl'

import React, { Component } from 'react'
import 'lodash'

export default class Cursor extends Component {
  render() {
    const { sel, onClick, us } = this.props
      , styles = (sel && !us)
        ? { top: sel.top
          , left: '50%'
          , marginLeft: (sel.hcenter - (document.body.clientWidth * .5)) + 'px' }
        : { display: 'none' }

    return (
      <div style={styles} onClick={onClick} className={'cursor'}>
        Write a note
      </div>
    )
  }
}
