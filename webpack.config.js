var webpack = require('webpack')
  , plugins = []
  , production = false

if (production) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    })
  )
}

module.exports = {
  entry: {
    //'notes-system': './lib/index.js',
    'example': './example/example.js',
    'tests': './tests/index.js'
  },
  output: {
    path: './compiled',
    filename: '[name].js',
    library: '[name]',
    libraryTarget: 'umd'
  },
  module: {
    loaders: [
      { test: /\.js$/ , loaders: [ 'jsx-loader', 'babel-loader' ] },
      { test: /\.css$/ , loader: 'style-loader!css-loader' },
      { test: /\.styl$/ , loader: 'style-loader!css-loader!stylus-loader' }
    ]
  },
  devtool: 'source-map',
  plugins: plugins
}
